#pragma once

#include <map>
#include <memory>

#include "../crossword_view/crossword_model.h"
#include "../dictionary/dictionary.h"

namespace solver {

using namespace crossword_view;

struct Site;
typedef std::shared_ptr<Site> SitePtr;

struct Intersection {
  size_t index;
  SitePtr intersected_site;
  size_t intersected_index;
};

struct Site {
  int id;
  size_t row;
  size_t column;
  enum Direction { HORIZONTAL, VERTICAL };
  Direction direction;
  size_t length;
  std::map<size_t, Intersection>
      intersections;  // <character index, intersecting site>
  Dictionary::UTF32PtrVectors
      domain;     // this is the initial domain. don't modify it!
  Anchor anchor;  // the original anchor is somewhat redundant but useful
};

// This represents state of a crossword.
// After puting a word on a crossword it gets added to the map of `words`.
// The domains of intersecting sites should be updated to reflect new
// constraints after placing letters at intersections.
struct State {
  std::map<SitePtr, Dictionary::UTF32Vector*> words;
  std::map<SitePtr, Dictionary::UTF32PtrVectors> domains;
  bool solvable;
  const Dictionary::UTF32PtrVectors& GetDomainForSite(const SitePtr& site);
};

class Solver {
 public:
  Solver(DictionaryPtr dictionary, CrosswordModelPtr model);

  virtual ~Solver();

  virtual void Solve() {}

 protected:
  DictionaryPtr dictionary_;
  CrosswordModelPtr model_;

  std::vector<SitePtr> sites_;

  static State PutWord(const State& state,
                       const SitePtr& site,
                       Dictionary::UTF32Vector* word);

  static SitePtr GetMostConstrainedSite(const State& state);

  State CreateStartingState();

  void SaveStateToModel(const State& state);

  // save state to model?
  // or return the state and let others worry about converting it to model

 private:
  void MakeSites();
};

typedef std::unique_ptr<Solver> SolverPtr;

}  // namespace solver
