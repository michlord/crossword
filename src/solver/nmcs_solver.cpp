#include "nmcs_solver.h"

#include <glibmm/ustring.h>
#include <algorithm>
#include <iostream>
#include <set>

#include "../utils/random.h"

namespace solver {

NMCSSolver::NMCSSolver(DictionaryPtr dictionary,
                       crossword_view::CrosswordModelPtr model,
                       EvaluationCB evaluation_cb)
    : Solver(dictionary, model), evaluation_cb_(evaluation_cb) {}

void NMCSSolver::Solve() {
  State state = CreateStartingState();
  // this might need to be retried a couple times before a solution is found
  // level is adjusted according to price/performance
  int score = Nested(state, 1);
  if (state.solvable) {
    std::cerr << "Solution has been found, has a score " << score << std::endl;
    SaveStateToModel(state);
  } else {
    std::cerr << "The best we could do is a state with score " << score
              << " and " << state.words.size() << " words" << std::endl;
  }
}

int NMCSSolver::Sample(State state) {
  while (!EndOfGame(state)) {
    SitePtr site = GetMostConstrainedSite(state);
    auto domain = state.GetDomainForSite(site);

    random_selector<> selector{};
    auto random_it = selector(domain);
    state = PutWord(state, site, random_it);
  }
  return evaluation_cb_(state);
}

int NMCSSolver::Nested(State& state, int level) {
  if (level <= 0)
      return Sample(state);

  while (!EndOfGame(state)) {
    SitePtr site = GetMostConstrainedSite(state);
    auto domain = state.GetDomainForSite(site);

    int best_score = -1;
    State best_state;

    for (Dictionary::UTF32Vector* word : domain) {
      State tmp_state = PutWord(state, site, word);
      int sample_score = Nested(tmp_state, level - 1);
      if (best_score < sample_score) {
        best_score = sample_score;
        best_state = std::move(tmp_state);
      }
    }

    state = std::move(best_state);
  }
  return evaluation_cb_(state);
}

bool NMCSSolver::EndOfGame(const State& state) {
  return state.words.size() == sites_.size() || !state.solvable;
}

int EvaluateUniqueWordsAndLetters(const State& state) {
  return state.words.size();
  /*
  // TODO: might also want to promote states with the greatest depth
  // but this is in part achieved by the unique words score

  size_t max_letters = 0;
  std::set<Dictionary::UTF32Vector*> words;
  for (auto it = state.words.begin(); it != state.words.end(); ++it) {
    Dictionary::UTF32Vector* word = it->second;
    max_letters += word->size();
    words.insert(word);
  }

  std::set<gunichar> letters;
  for (const Dictionary::UTF32Vector* word : words) {
    std::copy(word->begin(), word->end(),
              std::inserter(letters, letters.end()));
  }

  size_t multiplier = 1;
  while (multiplier < max_letters && multiplier < 10000000)
    multiplier *= 10;

  int score = words.size() * multiplier + letters.size();
  // this will spam a lot, remove later
//  std::cerr << "unique words=" << words.size()
//            << " unique letters=" << letters.size() << " score=" << score
//            << std::endl;
  return score;
  */
}

}  // namespace solver
