#pragma once

#include "solver.h"

namespace solver {


class BacktrackingSolver : public Solver {
public:
  BacktrackingSolver(DictionaryPtr dictionary, CrosswordModelPtr model);

  void Solve() override;

private:
  bool SolveRec(const State& state);
  State final_state_;
};

} // namespace solver
