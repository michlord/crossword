#pragma once

#include <functional>
#include "solver.h"

namespace solver {

int EvaluateUniqueWordsAndLetters(const State& state);

class NMCSSolver : public Solver {
 public:
  typedef std::function<int(const State&)> EvaluationCB;

  NMCSSolver(DictionaryPtr dictionary,
             CrosswordModelPtr model,
             EvaluationCB evaluation_cb = EvaluateUniqueWordsAndLetters);
  void Solve() override;

 private:
  int Sample(State state);
  int Nested(State &state, int level);
  bool EndOfGame(const State& state);


  EvaluationCB evaluation_cb_;
  State final_state_;
};

}  // namespace solver
