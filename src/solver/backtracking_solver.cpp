#include "backtracking_solver.h"

namespace solver {

BacktrackingSolver::BacktrackingSolver(
    DictionaryPtr dictionary,
    crossword_view::CrosswordModelPtr model)
    : Solver(dictionary, model) {}

void BacktrackingSolver::Solve() {
  State state = CreateStartingState();
  bool res = SolveRec(state);
  if (res)
    SaveStateToModel(final_state_);
}

bool BacktrackingSolver::SolveRec(const State &state)
{
  if (state.words.size() == sites_.size())
  {
    final_state_ = state;
    return true;
  }

  SitePtr site = GetMostConstrainedSite(state);
  auto it = state.domains.find(site);
  if (it == state.domains.end())
    return false;
  for (Dictionary::UTF32Vector* word : it->second) {
    State new_state = PutWord(state, site, word);
    if (!new_state.solvable)
      continue;
    if (SolveRec(new_state))
      return true;
  }
  return false;
}

} // namespace solver

