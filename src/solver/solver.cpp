#include "solver.h"

#include <algorithm>
#include <utility>
#include <iostream>

namespace solver {

namespace {
Site::Direction ConvertAnchorDirection(Anchor::Direction direction) {
  switch (direction) {
    case Anchor::HORIZONTAL:
      return Site::HORIZONTAL;
    case Anchor::VERTICAL:
      return Site::VERTICAL;
  }
  return Site::HORIZONTAL;
}
}  // namespace

Solver::Solver(DictionaryPtr dictionary, CrosswordModelPtr model)
    : dictionary_(dictionary), model_(model) {
  MakeSites();
}

Solver::~Solver() {}

State Solver::PutWord(const State& state,
                      const SitePtr& site,
                      Dictionary::UTF32Vector* word) {
  State new_state = state;
  new_state.words[site] = word;
  new_state.domains[site] = {word};
  new_state.solvable = true;

  // I wonder if this will have some bugs... make unit tests!!!
  for (const auto& pair : site->intersections) {
    const Intersection& intersection = pair.second;

    auto it = state.domains.find(intersection.intersected_site);
    if (it == state.domains.end())
      continue;

    new_state.domains[intersection.intersected_site] =
        GetWordsConstrainedByCharacter(it->second, (*word)[intersection.index],
                                       intersection.intersected_index);
    // if any of the domains becomes empty then the crossword in this state
    // is unsolvable
    if (new_state.domains[intersection.intersected_site].empty())
      new_state.solvable = false;
  }

  return new_state;
}

SitePtr Solver::GetMostConstrainedSite(const State& state) {
  // This should only search domains for sites which don't
  // have an assigned word.

  std::vector<std::pair<SitePtr, Dictionary::UTF32PtrVectors>> domains;

  std::copy_if(state.domains.begin(), state.domains.end(),
               std::back_inserter(domains),
               [&state](const std::pair<SitePtr, Dictionary::UTF32PtrVectors>& pair) {
                 return state.words.find(pair.first) == state.words.end();
               });

  auto it = std::min_element(
      domains.begin(), domains.end(),
      [](const std::pair<SitePtr, Dictionary::UTF32PtrVectors>& lhs,
         const std::pair<SitePtr, Dictionary::UTF32PtrVectors>& rhs) {
        return lhs.second.size() < rhs.second.size();
      });

  if (it == domains.end())
    return nullptr;

  return it->first;
}

State Solver::CreateStartingState() {
  State state;
  state.solvable = true;
  for (const SitePtr& site : sites_) {
    state.domains[site] = site->domain;
  }
  return state;
}

void Solver::SaveStateToModel(const State& state) {
  for (const auto& pair : state.words) {
    model_->SetWord(pair.first->anchor, *(pair.second));
  }
}

namespace {

bool AddIntersection(SitePtr& vertical, SitePtr& horizontal) {
  if (vertical->column < horizontal->column ||
      vertical->column >= horizontal->column + horizontal->length)
    return false;
  if (vertical->row + vertical->length <= horizontal->row ||
      vertical->row > horizontal->row)
    return false;
  Intersection vertical_intersection;
  Intersection horizontal_intersection;

  vertical_intersection.index = horizontal->row - vertical->row;
  vertical_intersection.intersected_index =
      vertical->column - horizontal->column;
  vertical_intersection.intersected_site = horizontal;
  vertical->intersections[vertical_intersection.index] = vertical_intersection;

  horizontal_intersection.index = vertical->column - horizontal->column;
  horizontal_intersection.intersected_index = horizontal->row - vertical->row;
  horizontal_intersection.intersected_site = vertical;
  horizontal->intersections[horizontal_intersection.index] =
      horizontal_intersection;

  return true;
}

}  // namespace

void Solver::MakeSites() {
  std::vector<Anchor> anchors = model_->GetAnchors();

  for (const Anchor& anchor : anchors) {
    SitePtr site = SitePtr(new Site());
    site->row = anchor.row;
    site->column = anchor.column;
    site->direction = ConvertAnchorDirection(anchor.direction);
    site->length = anchor.length;
    std::vector<gunichar> word = model_->GetWord(anchor);
    site->domain = dictionary_->GetWordsByQuery(word);
    site->anchor = anchor;
    sites_.push_back(site);
  }

  for (SitePtr& vertical_site : sites_) {
    if (vertical_site->direction != Site::VERTICAL)
      continue;
    for (SitePtr& horizontal_site : sites_) {
      if (horizontal_site->direction != Site::HORIZONTAL)
        continue;
      AddIntersection(vertical_site, horizontal_site);
    }
  }
}

const Dictionary::UTF32PtrVectors &State::GetDomainForSite(const SitePtr &site) {
  auto domains_it = domains.find(site);
  if (domains_it == domains.end()) {
    std::cerr << "Couldn't find a domain for a site: " << site.get()
              << std::endl;
    exit(1);
  }
  return domains_it->second;
}

}  // namespace solver
