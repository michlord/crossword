#include "simple_dictionary.h"

#include <glibmm/ustring.h>
#include <fstream>
#include <iostream>

namespace {
bool WildcardMatch(const SimpleDictionary::UTF32Vector& wildcard,
                   const SimpleDictionary::UTF32Vector& word) {
  return std::equal(wildcard.begin(), wildcard.end(), word.begin(), word.end(),
                    [](const gunichar& lhs, const gunichar& rhs) {
                      return lhs == '*' || lhs == rhs;
                    });
}

}  // namespace

Dictionary::UTF32PtrVectors SimpleDictionary::GetWordsByQuery(
    UTF32Vector query) {
  SimpleDictionary::UTF32PtrVectors result;
  size_t size = query.size();

  auto range = words_.equal_range(size);
  for (auto it = range.first; it != range.second; ++it) {
    if (WildcardMatch(query, it->second))
      result.push_back(&(it->second));
  }

  return result;
}

SimpleDictionary::SimpleDictionary() {}

SimpleDictionary::SimpleDictionary(const std::vector<Glib::ustring>& words) {
  for (auto word : words) {
    UTF32Vector vector(word.begin(), word.end());
    words_.insert(std::make_pair(vector.size(), vector));
  }
}

SimpleDictionaryPtr SimpleDictionary::CreateFromFile(const char* file_path) {
  std::ifstream in;
  Glib::ustring buffer;
  SimpleDictionaryPtr result = SimpleDictionaryPtr(new SimpleDictionary());
  in.open(file_path, std::ifstream::in);
  if (!in) {
    std::cerr << "File not found " << file_path << std::endl;
    exit(1);
  }
  while (in >> buffer) {
    UTF32Vector vector(buffer.begin(), buffer.end());
    result->words_.insert(std::make_pair(vector.size(), vector));
  }
  return result;
}
