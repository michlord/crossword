#pragma once

#include <glibmm/ustring.h>
#include <memory>
#include <vector>

// TODO: provide an efficient dictionary based on radix tree
class Dictionary {
 public:
  typedef std::vector<gunichar> UTF32Vector;
  typedef std::vector<UTF32Vector> UTF32Vectors;
  typedef std::vector<UTF32Vector*> UTF32PtrVectors;
  virtual UTF32PtrVectors GetWordsByQuery(UTF32Vector query) = 0;
  virtual ~Dictionary() {}
};

typedef std::shared_ptr<Dictionary> DictionaryPtr;

Dictionary::UTF32PtrVectors GetWordsConstrainedByCharacter(
    const Dictionary::UTF32PtrVectors& words,
    gunichar character,
    size_t position);


Dictionary::UTF32Vectors ToUTF32Vectors(std::vector<Glib::ustring> words);
