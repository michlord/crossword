#include "simple_dictionary.h"

#include <gtest/gtest.h>

// TODO: make parametrized test with many strings
// - different word sets
// - various dictionary implementations
// - various queries and expected results

TEST(SimpleDictionaryTest, Query) {
  SimpleDictionary dictionary({"michal", "ala", "kot", "alahambra", "michalina",
                               "sylwester", "miszel", "milkal"});

  Glib::ustring query_string = "mi***l";
  Dictionary::UTF32Vector query(query_string.begin(), query_string.end());

  auto result = dictionary.GetWordsByQuery(query);
  Dictionary::UTF32Vectors result_copy;
  for (Dictionary::UTF32Vector* vec : result) {
    result_copy.push_back(*vec);
  }

  const auto expected = ToUTF32Vectors({"michal", "miszel", "milkal"});


  ASSERT_EQ(result_copy, expected);
}
