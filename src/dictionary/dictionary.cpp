#include "dictionary.h"

#include <algorithm>

Dictionary::UTF32PtrVectors GetWordsConstrainedByCharacter(
    const Dictionary::UTF32PtrVectors& words,
    gunichar character,
    size_t position) {
  Dictionary::UTF32PtrVectors result;
  std::copy_if(words.begin(), words.end(), std::back_inserter(result),
               [character, position](const Dictionary::UTF32Vector* x) {
                 return (*x)[position] == character;
               });
  return result;
}

Dictionary::UTF32Vectors ToUTF32Vectors(std::vector<Glib::ustring> words) {
  Dictionary::UTF32Vectors result;
  for (auto word : words) {
    Dictionary::UTF32Vector buffer(word.begin(), word.end());
    result.push_back(buffer);
  }
  return result;
}
