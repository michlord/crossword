#pragma once

#include "dictionary.h"

#include <map>
#include <memory>

class SimpleDictionary;
typedef std::shared_ptr<SimpleDictionary> SimpleDictionaryPtr;

class SimpleDictionary : public Dictionary {
 public:
  UTF32PtrVectors GetWordsByQuery(UTF32Vector query) override;


  SimpleDictionary();
  SimpleDictionary(const std::vector<Glib::ustring> &words);
  static SimpleDictionaryPtr CreateFromFile(const char* file_path);

 protected:
  std::multimap<int, UTF32Vector> words_;
};


