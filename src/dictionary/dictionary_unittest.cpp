#include "dictionary.h"

#include <gtest/gtest.h>

TEST(DictionaryTest, Constrain) {
  auto input =
      ToUTF32Vectors({"michal", "miszel", "milkal", "mi", "michalina",
                      "michalangelo", "marek", "mikolaj", "micky", "mic"});

  Dictionary::UTF32PtrVectors input_ptr;
  for (Dictionary::UTF32Vector& utf32vector : input) {
    input_ptr.push_back(&utf32vector);
  }

  std::vector<Dictionary::UTF32Vector*> expected{
      input_ptr[0], input_ptr[4], input_ptr[5], input_ptr[8], input_ptr[9]};

  auto result = GetWordsConstrainedByCharacter(input_ptr, 'c', 2);

  ASSERT_EQ(result, expected);
}
