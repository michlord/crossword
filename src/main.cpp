/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <csignal>
#include <iostream>

#include <gtkmm/application.h>
#include <gtkmm/builder.h>

#include "window/main_window.h"

#include <config.h>

const char kGladeFilePath[] = SOURCE_ROOT "/glade/main_window.glade";

void handler(int sig) {
  std::cerr << "Received a SIGSEGV " << sig << std::endl;
  exit(1);
}

int main(int argc, char* argv[]) {
  signal(SIGSEGV, handler);

  auto app = Gtk::Application::create(argc, argv, "com.example.crossword");
  auto builder = Gtk::Builder::create();
  ApplicationWindow* app_window = nullptr;

  builder->add_from_file(kGladeFilePath);
  builder->get_widget_derived("ApplicationWindow", app_window);

  if (app_window) {
    app->run(*app_window);
  }

  delete app_window;
  return 0;
}
