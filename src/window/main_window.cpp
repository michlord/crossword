#include "main_window.h"

#include <chrono>
#include <iostream>
#include <string>
#include <set>

#include <gtkmm/box.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/object.h>

#include "../crossword_view/crossword_model_table.h"
#include "../dictionary/simple_dictionary.h"
#include "../solver/backtracking_solver.h"
#include "../solver/nmcs_solver.h"
#include "../utils/load_data.h"

ApplicationWindow::ApplicationWindow(BaseObjectType* cobject,
                                     const Glib::RefPtr<Gtk::Builder>& builder)
    : Gtk::Window(cobject), builder_(builder) {
  crossword_view = Gtk::manage(new crossword_view::CrosswordView());
  Gtk::Box* box = nullptr;
  builder_->get_widget("MainBox", box);
  box->pack_end(*crossword_view);
  auto model =
      crossword_view::CrosswordModelPtr(new crossword_view::CrosswordModelTable(
          load_data::RandomData(6, 4), 6, 4));
  crossword_view->SetModel(std::move(model));
  crossword_view->show();

  builder->get_widget("NewButton", new_button_);
  builder->get_widget("OpenButton", open_button_);
  builder->get_widget("SaveButton", save_button_);
  builder->get_widget("SolveButton", solve_button_);
  builder->get_widget("PreferencesButton", preferences_button_);

  new_button_->signal_clicked().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_new_button_clicked));
  open_button_->signal_clicked().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_open_button_clicked));
  save_button_->signal_clicked().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_save_button_clicked));
  solve_button_->signal_clicked().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_solve_button_clicked));
  preferences_button_->signal_clicked().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_preferences_button_clicked));

  builder->get_widget("MainHeaderBar", header_bar_);
  builder->get_widget("MainStack", main_stack_);
  builder->get_widget("PreferencesListBox", preferences_listbox_);

  InitPreferencesListBox();
}

ApplicationWindow::~ApplicationWindow() {}

void ApplicationWindow::on_new_button_clicked() {
  std::cerr << "New button clicked" << std::endl;
  header_bar_->set_title("Untitled");
  crossword_view->SetModel(nullptr);
}

void ApplicationWindow::on_open_button_clicked() {
  std::cerr << "Open button clicked" << std::endl;

  Gtk::FileChooserDialog dialog("Please choose a file",
                                Gtk::FILE_CHOOSER_ACTION_OPEN);
  dialog.set_transient_for(*this);

  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Open", Gtk::RESPONSE_OK);

  auto filter_text = Gtk::FileFilter::create();
  filter_text->set_name("Text files");
  filter_text->add_mime_type("text/plain");
  dialog.add_filter(filter_text);

  auto filter_any = Gtk::FileFilter::create();
  filter_any->set_name("Any files");
  filter_any->add_pattern("*");
  dialog.add_filter(filter_any);

  // Show the dialog and wait for a user response:
  int result = dialog.run();

  // Handle the response:
  switch (result) {
    case (Gtk::RESPONSE_OK): {
      std::cout << "Open clicked." << std::endl;

      // Notice that this is a std::string, not a Glib::ustring.
      std::string filename = dialog.get_filename();
      std::cout << "File selected: " << filename << std::endl;
      header_bar_->set_title(filename);
      auto model =
          crossword_view::CrosswordModelTable::CreateFromFile(filename.c_str());
      crossword_view->SetModel(std::move(model));
      break;
    }
    case (Gtk::RESPONSE_CANCEL): {
      std::cout << "Cancel clicked." << std::endl;
      break;
    }
    default: {
      std::cout << "Unexpected button clicked." << std::endl;
      break;
    }
  }
}

void ApplicationWindow::on_save_button_clicked() {
  std::cerr << "Save button clicked" << std::endl;
  header_bar_->set_title("Name of the saved file");

  Gtk::FileChooserDialog dialog("Please choose a file",
                                Gtk::FILE_CHOOSER_ACTION_SAVE);
  dialog.set_transient_for(*this);

  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Save", Gtk::RESPONSE_OK);

  int result = dialog.run();

  switch (result) {
    case (Gtk::RESPONSE_OK): {
      std::cout << "Save clicked." << std::endl;

      std::string filename = dialog.get_filename();
      std::cout << "File selected: " << filename << std::endl;
      header_bar_->set_title(filename);

      load_data::SaveAsTxt(*crossword_view->GetModel(), filename.c_str());

      break;
    }
    case (Gtk::RESPONSE_CANCEL): {
      std::cout << "Cancel clicked." << std::endl;
      break;
    }
    default: {
      std::cout << "Unexpected button clicked." << std::endl;
      break;
    }
  }
}

void ApplicationWindow::on_preferences_button_clicked() {
  std::cerr << "Preferences button clicked" << std::endl;
  if (main_stack_->get_visible_child_name() == "Preferences") {
    main_stack_->set_visible_child("CrosswordEditor");
    preferences_button_->set_label("Preferences");
  } else {
    main_stack_->set_visible_child("Preferences");
    preferences_button_->set_label("Editor");
  }
}

void ApplicationWindow::on_dictionary_path_changed() {
  std::string file_name = dictionary_chooser_button_->get_filename();
  std::cerr << "on_dictionary_path_changed() filename=" << file_name
            << std::endl;

  dictionary_ = SimpleDictionary::CreateFromFile(file_name.c_str());
}

namespace {
int CountUniqueWords(const crossword_view::CrosswordModelPtr& model_) {
  auto anchors = model_->GetAnchors();
  std::set<Dictionary::UTF32Vector> words;
  for (auto it = anchors.begin(); it != anchors.end(); ++it) {
    Dictionary::UTF32Vector word = model_->GetWord(*it);
    words.insert(word);
  }
  return words.size();
}
}

void ApplicationWindow::on_solve_button_clicked() {
  std::cerr << "on_solve_button_clicked()" << std::endl;

  if (!dictionary_) {
    Gtk::MessageDialog message("Dictionary is missing");
    message.set_secondary_text("Please specify file path in preferences.");
    message.run();
    return;
  }

  if (combobox_solver_->get_active_text() == "Backtracking")
    solver_ = solver::SolverPtr(new solver::BacktrackingSolver(
        dictionary_, crossword_view->GetModel()));
  if (combobox_solver_->get_active_text() == "NMCS")
    solver_ = solver::SolverPtr(
        new solver::NMCSSolver(dictionary_, crossword_view->GetModel()));

  if (solver_) {
    std::chrono::time_point<std::chrono::system_clock> start_time =
        std::chrono::system_clock::now();
    solver_->Solve();
    std::chrono::time_point<std::chrono::system_clock> end_time =
        std::chrono::system_clock::now();
    double duration_seconds =
        std::chrono::duration_cast<std::chrono::milliseconds>(end_time -
                                                              start_time)
            .count() /
        1000.0;
    std::cout << "Solver took " << duration_seconds << " seconds" << std::endl;
    std::cout << "There are " << CountUniqueWords(crossword_view->GetModel()) << " unique words" << std::endl;
    crossword_view->ForceRedraw();
  }
}

void ApplicationWindow::InitPreferencesListBox() {
  preferences_listbox_->set_selection_mode(Gtk::SELECTION_NONE);

  auto row = Gtk::manage(new Gtk::ListBoxRow());
  dictionary_chooser_button_ = Gtk::manage(new Gtk::FileChooserButton());
  auto label = Gtk::manage(new Gtk::Label());
  label->set_markup("<b>Dictionary path</b>");
  auto hbox = Gtk::manage(new Gtk::HBox(false, 100));
  hbox->pack_start(*label, false, 0);
  hbox->pack_start(*dictionary_chooser_button_);
  row->add(*hbox);
  preferences_listbox_->add(*row);

  row = Gtk::manage(new Gtk::ListBoxRow());
  label = Gtk::manage(new Gtk::Label());
  label->set_markup("<b>Solver</b>");
  hbox = Gtk::manage(new Gtk::HBox(false, 100));
  combobox_solver_ = Gtk::manage(new Gtk::ComboBoxText());
  combobox_solver_->append("Backtracking");
  combobox_solver_->append("NMCS");
  combobox_solver_->set_active_text("Backtracking");

  hbox->pack_start(*label, false, 0);
  hbox->pack_start(*combobox_solver_);
  row->add(*hbox);
  preferences_listbox_->add(*row);

  preferences_listbox_->show_all();

  dictionary_chooser_button_->signal_file_set().connect(
      sigc::mem_fun(*this, &ApplicationWindow::on_dictionary_path_changed));
}
