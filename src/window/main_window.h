#pragma once

#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/window.h>
#include <gtkmm/stack.h>
#include <gtkmm/listbox.h>
#include <gtkmm/filechooserbutton.h>
#include <gtkmm/comboboxtext.h>

#include "../crossword_view/crossword_view.h"
#include "../solver/solver.h"
#include "../dictionary/dictionary.h"

class ApplicationWindow : public Gtk::Window {
 public:
  ApplicationWindow(BaseObjectType* cobject,
                    const Glib::RefPtr<Gtk::Builder>& builder);
  virtual ~ApplicationWindow();

 protected:
  Glib::RefPtr<Gtk::Builder> builder_;
  Gtk::Button* new_button_;
  Gtk::Button* open_button_;
  Gtk::Button* save_button_;
  Gtk::Button* solve_button_;
  Gtk::Button* preferences_button_;
  Gtk::Stack* main_stack_;
  Gtk::HeaderBar* header_bar_;
  Gtk::ListBox* preferences_listbox_;
  Gtk::FileChooserButton* dictionary_chooser_button_;
  Gtk::ComboBoxText* combobox_solver_;


  crossword_view::CrosswordView* crossword_view;
  solver::SolverPtr solver_;
  DictionaryPtr dictionary_;

  void on_new_button_clicked();
  void on_open_button_clicked();
  void on_save_button_clicked();
  void on_preferences_button_clicked();

  void on_dictionary_path_changed();
  void on_solve_button_clicked();

  void InitPreferencesListBox();
};
