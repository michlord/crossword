#pragma once

#include <vector>
#include <glibmm/unicode.h>

#include "../crossword_view/crossword_model.h"

namespace load_data {

typedef std::vector<gunichar> UTF32Vector;

UTF32Vector RandomData(size_t width, size_t height);

void SaveAsTxt(const crossword_view::CrosswordModel& model, const char* file_path);


} // namespace load_data
