#include "load_data.h"
#include <cstdlib>
#include <iostream>
#include <fstream>

namespace load_data {

UTF32Vector RandomData(size_t width, size_t height) {
  UTF32Vector vec;
  const size_t total = width * height;

  for (size_t i = 0; i < total; ++i) {
    vec.push_back(std::rand() % ('z' - 'a') + 'a');
  }

  for (size_t i = 0; i < total; ++i) {
    if (std::rand() % 4 == 1)
      vec[i] = '#';
  }

  return vec;
}

void SaveAsTxt(const crossword_view::CrosswordModel& model, const char *file_path) {
  std::fstream fout;
  fout.open(file_path, std::ios_base::out);
  if (!fout) {
    std::cerr << "Could not save to " << file_path << std::endl;
    exit(1);
  }
  fout << model;
}

}  // namespace load_data
