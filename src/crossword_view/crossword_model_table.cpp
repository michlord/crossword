#include "crossword_model_table.h"

#include <algorithm>
#include <fstream>
#include <iostream>

namespace crossword_view {

CrosswordModelTable::CrosswordModelTable(std::vector<gunichar> data,
                                         size_t width,
                                         size_t height)
    : width_(width), height_(height) {
  int current_row = 0;
  int current_column = 0;
  for (gunichar character : data) {
    CellInfo cell_info;
    cell_info.is_clear = false;
    cell_info.type = CellInfo::NORMAL;
    cell_info.character = character;
    cell_info.row = current_row;
    cell_info.column = current_column;

    if (character == '#')
      cell_info.type = CellInfo::WALL;

    if (character == '_')
      cell_info.is_clear = true;

    data_.push_back(cell_info);

    current_column += 1;
    if (current_column >= width) {
      current_column = 0;
      current_row += 1;
    }
  }

  RebuildAnchors();
}

size_t CrosswordModelTable::GetWidth() const {
  return width_;
}

size_t CrosswordModelTable::GetHeight() const {
  return height_;
}

CellInfo CrosswordModelTable::GetCell(size_t row, size_t column) const {
  return data_[row * width_ + column];
}

void CrosswordModelTable::SetCharacter(size_t row,
                                       size_t column,
                                       gunichar character) {
  CellInfo& info = data_[row * width_ + column];
  info.character = character;
  info.type = CellInfo::NORMAL;
  info.is_clear = false;
}

Selection CrosswordModelTable::GetSelection(
    size_t row,
    size_t column,
    Selection::Direction direction) const {
  Selection result;
  result.direction = direction;

  if (!IsInRange(row, column) || GetCell(row, column).type == CellInfo::WALL)
    return result;

  auto& cells = result.cells;

  size_t start;

  // get whole row or column
  if (direction == Selection::HORIZONTAL) {
    for (size_t i = 0; i < width_; ++i) {
      cells.push_back(GetCell(row, i));
    }
    start = column;
  } else {
    for (size_t i = 0; i < height_; ++i) {
      cells.push_back(GetCell(i, column));
    }
    start = row;
  }

  // remove elements that fall outside selection
  std::vector<CellInfo>::iterator it = cells.begin() + start;

  auto f = std::find_if(it, cells.end(), [](const CellInfo& c) {
    return c.type == CellInfo::WALL;
  });
  cells.erase(f, cells.end());

  while (it != cells.begin() && it->type != CellInfo::WALL)
    --it;
  if (it->type == CellInfo::WALL)
    ++it;

  cells.erase(cells.begin(), it);

  result.current_cell = std::find_if(
      cells.begin(), cells.end(), [row, column](const CellInfo& c) {
        return c.row == row && c.column == column;
      });

  return result;
}

std::vector<Anchor> CrosswordModelTable::GetAnchors() const {
  return anchors_;
}

std::vector<Anchor> CrosswordModelTable::GetVerticalAnchors() const {
  return vertical_anchors_;
}

std::vector<Anchor> CrosswordModelTable::GetHorizontalAnchors() const {
  return horizontal_anchors_;
}

bool CrosswordModelTable::SetWord(const Anchor& anchor,
                                  const std::vector<gunichar>& word) {
  if (anchor.direction == Anchor::HORIZONTAL) {
    for (size_t column = anchor.column, i = 0;
         column < anchor.length + anchor.column && i < word.size();
         ++column, ++i) {
      CellInfo& info = data_[anchor.row * width_ + column];
      info.character = word[i];
      info.is_clear = false;
    }
    return true;
  } else {
    for (size_t row = anchor.row, i = 0;
         row < anchor.length + anchor.row && i < word.size(); ++row, ++i) {
      CellInfo& info = data_[row * width_ + anchor.column];
      info.character = word[i];
      info.is_clear = false;
    }
    return true;
  }
  return false;
}

std::vector<gunichar> CrosswordModelTable::GetWord(const Anchor& anchor) const {
  std::vector<gunichar> result;
  if (anchor.direction == Anchor::HORIZONTAL) {
    for (size_t column = anchor.column; column < anchor.length + anchor.column;
         ++column) {
      const CellInfo& info = data_[anchor.row * width_ + column];
      if (info.is_clear)
        result.push_back('*');
      else
        result.push_back(info.character);
    }
  } else {
    for (size_t row = anchor.row; row < anchor.length + anchor.row; ++row) {
      const CellInfo& info = data_[row * width_ + anchor.column];
      if (info.is_clear)
        result.push_back('*');
      else
        result.push_back(info.character);
    }
  }
  return result;
}

void CrosswordModelTable::ClearCell(size_t row, size_t column) {
  data_[row * width_ + column].is_clear = true;
}

CrosswordModelPtr CrosswordModelTable::CreateFromFile(const char* file_path) {
  std::ifstream in;
  in.open(file_path, std::ifstream::in);
  if (!in) {
    std::cerr << "File not found " << file_path << std::endl;
    exit(1);
  }

  size_t width, height;

  if (!(in >> width >> height)) {
    std::cerr << "Couldn't parse board dimensions" << std::endl;
    exit(1);
  }
  in.ignore();

  std::cout << "Width: " << width << " Height: " << height << std::endl;

  std::vector<gunichar> data;
  std::string buffer;
  Glib::ustring line;
  for (size_t row = 0; row < height; ++row) {
    if (!(in >> buffer)) {
      std::cerr << "Error reading board structure" << std::endl;
      exit(1);
    }
    line = Glib::ustring(std::move(buffer));

    if (line.size() < width) {
      std::cerr << "Board has smaller width than declared" << std::endl;
    }

    data.insert(data.end(), line.begin(), line.end());
  }

  return CrosswordModelPtr(new CrosswordModelTable(data, width, height));
}

std::vector<CellInfo> CrosswordModelTable::GetLine(
    size_t index,
    Anchor::Direction direction) {
  std::vector<CellInfo> result;
  if (direction == Anchor::HORIZONTAL) {
    const size_t row = index;
    for (size_t col = 0; col < width_; ++col)
      result.push_back(data_[row * width_ + col]);
  } else {
    const size_t col = index;
    for (size_t row = 0; row < height_; ++row)
      result.push_back(data_[row * width_ + col]);
  }
  return result;
}

namespace {

std::vector<std::vector<CellInfo>> Tokenize(const std::vector<CellInfo>& line) {
  std::vector<std::vector<CellInfo>> tokens;
  for (const CellInfo& info : line) {
    if (tokens.empty())
      tokens.push_back({info});
    else if (tokens.back().back().type != info.type)
      tokens.push_back({info});
    else
      tokens.back().push_back({info});
  }
  return tokens;
}

std::vector<Anchor> TokensToAnchors(
    size_t index,
    const std::vector<std::vector<CellInfo>>& tokens,
    Anchor::Direction direction) {
  std::vector<Anchor> anchors;
  size_t len = 0;

  for (const auto& token : tokens) {
    const CellInfo& info = token.front();
    if (info.type != CellInfo::WALL && token.size() > 1) {
      Anchor anchor;
      anchor.id = 0;  // todo
      anchor.row = direction == Anchor::HORIZONTAL ? index : len;
      anchor.column = direction == Anchor::VERTICAL ? index : len;
      anchor.direction = direction;
      anchor.length = token.size();
      anchors.push_back(anchor);
    }
    len += token.size();

    for (const auto& cell : token) {
      std::cout << (cell.type == CellInfo::WALL ? "#" : ".");
    }
    std::cout << ", ";
  }
  std::cout << std::endl;
  return anchors;
}

}  // namespace

std::vector<Anchor> CrosswordModelTable::MakeAnchors(
    Anchor::Direction direction) {
  std::vector<Anchor> anchors;

  size_t limit = direction == Anchor::HORIZONTAL ? height_ : width_;

  for (size_t index = 0; index < limit; ++index) {
    auto line = GetLine(index, direction);
    auto tokens = Tokenize(line);
    auto line_anchors = TokensToAnchors(index, tokens, direction);
    anchors.insert(anchors.end(), line_anchors.begin(), line_anchors.end());
  }

  std::cout << "anchors: ";
  for (const auto& anchor : anchors) {
    std::cout << "[row=" << anchor.row << " col=" << anchor.column
              << " len=" << anchor.length << "], ";
  }
  std::cout << std::endl;
  return anchors;
}

void CrosswordModelTable::RebuildAnchors() {
  anchors_.clear();
  horizontal_anchors_ = MakeAnchors(Anchor::HORIZONTAL);
  vertical_anchors_ = MakeAnchors(Anchor::VERTICAL);

  anchors_.insert(anchors_.end(), vertical_anchors_.begin(),
                  vertical_anchors_.end());
  anchors_.insert(anchors_.end(), horizontal_anchors_.begin(),
                  horizontal_anchors_.end());
}

}  // namespace crossword_view
