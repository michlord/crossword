#pragma once

#include <memory>
#include <ostream>
#include <vector>

#include "cell_info.h"
#include "selection.h"

namespace crossword_view {

class CrosswordModel {
 public:
  virtual size_t GetWidth() const = 0;
  virtual size_t GetHeight() const = 0;
  virtual CellInfo GetCell(size_t row, size_t column) const = 0;
  virtual void SetCharacter(size_t row, size_t column, gunichar character) = 0;
  virtual void ClearCell(size_t row, size_t column) = 0;

  // Gets a word build from cells that you would expect to get
  // when you click with a mouse on a cell with the specified row and column.
  // You can specify the direction of the word on the crossword.
  virtual Selection GetSelection(size_t row,
                                 size_t column,
                                 Selection::Direction direction) const = 0;

  virtual std::vector<Anchor> GetAnchors() const = 0;
  virtual std::vector<Anchor> GetVerticalAnchors() const = 0;
  virtual std::vector<Anchor> GetHorizontalAnchors() const = 0;

  // Sets a word starting at anchor. Returns true on success.
  virtual bool SetWord(const Anchor& anchor, const std::vector<gunichar>& word) = 0;
  virtual std::vector<gunichar> GetWord(const Anchor& anchor) const = 0;

  virtual ~CrosswordModel();

 protected:
  bool IsInRange(size_t row, size_t column) const;

  friend std::ostream& operator<<(std::ostream& os,
                                  const CrosswordModel& model);
};

typedef std::shared_ptr<CrosswordModel> CrosswordModelPtr;

}  // namespace crossword_view
