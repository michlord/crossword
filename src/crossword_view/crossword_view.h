#pragma once

#include <memory>

#include <gtkmm/drawingarea.h>
#include <gtkmm/popover.h>

#include "crossword_model.h"

// todo: refactor implementation of on_draw, on_button_press_event, ...
// to use an appropriate handler based on the current mode of operation
// (editing words, editing structure). Might consider using strategy
// pattern for this.
namespace crossword_view {

class CrosswordView : public Gtk::DrawingArea {
 public:
  CrosswordView();
  virtual ~CrosswordView();

  void SetModel(CrosswordModelPtr model);
  const CrosswordModelPtr GetModel() const;

  // todo: remove this function. Either use a signal to notify that
  // the model has changed or provide a function for changing model
  // that also performs the redraw.
  void ForceRedraw();

 protected:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  bool on_button_press_event(GdkEventButton * button_event) override;
  bool on_key_press_event(GdkEventKey * key_event) override;


  virtual Gdk::Rectangle GetSelectionRectangle();


  CrosswordModelPtr model_;
  Selection selection_;

  Gtk::Popover popover_;
};

} // namespace crossword_view
