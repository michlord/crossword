#pragma once

#include "cell_info.h"

namespace crossword_view {

// it is bad to do a memberwise copy of this struct
// because iterator gets invalidated
struct Selection {
  enum Direction { HORIZONTAL, VERTICAL };
  Direction direction;
  std::vector<CellInfo> cells;
  std::vector<CellInfo>::iterator current_cell;
  Selection() : direction(HORIZONTAL), cells(), current_cell() {}
};

} // namespace crossword_view

