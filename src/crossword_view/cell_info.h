#pragma once

#include <glibmm/ustring.h>

namespace crossword_view {

struct CellInfo {
  enum CellType { WALL, NORMAL };

  CellType type;
  gunichar character;

  size_t row;
  size_t column;
  bool is_clear;
};

struct Anchor {
  int id;
  size_t row;
  size_t column;
  enum Direction {HORIZONTAL, VERTICAL};
  Direction direction;
  size_t length;
};

} // namespace crossword_view
