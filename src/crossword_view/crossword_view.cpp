#include "crossword_view.h"
#include <glibmm/ustring.h>
#include <iostream>

#include <gdk/gdkkeysyms.h>
#include <gtkmm/button.h>

namespace crossword_view {

CrosswordView::CrosswordView() : popover_(*this) {
  add_events(Gdk::BUTTON_PRESS_MASK);
  add_events(Gdk::KEY_PRESS_MASK);
  set_can_focus(true);
  popover_.set_position(Gtk::POS_BOTTOM);
  popover_.set_modal(false);
  Gtk::Button* button = Gtk::manage(new Gtk::Button("Test"));
  popover_.add(*button);
  button->show();
}

CrosswordView::~CrosswordView() {}

void CrosswordView::SetModel(CrosswordModelPtr model) {
  model_ = std::move(model);
  selection_.cells.clear();
  queue_draw();
}

const CrosswordModelPtr CrosswordView::GetModel() const {
  return model_;
}

void CrosswordView::ForceRedraw() {
  queue_draw();
}

bool CrosswordView::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

  // Draw a black background
  cr->set_source_rgb(0.0, 0.0, 0.0);
  cr->rectangle(0, 0, width, height);
  cr->fill();

  if (!model_)
    return true;

  const int cells_horizontal = model_->GetWidth();
  const int cells_vertical = model_->GetHeight();

  const int border = 2;
  const int cell_size =
      std::min(width / cells_horizontal, height / cells_vertical) - border;

  // Draw white squares

  for (int i = 0; i < cells_vertical; ++i) {
    for (int j = 0; j < cells_horizontal; ++j) {
      int oj = j * (cell_size + border);
      int oi = i * (cell_size + border);

      const CellInfo cell = model_->GetCell(i, j);

      switch (cell.type) {
        case CellInfo::NORMAL:
          cr->set_source_rgb(1.0, 1.0, 1.0);
          cr->rectangle(oj + border, oi + border, cell_size, cell_size);
          cr->fill();
          break;
        case CellInfo::WALL:
          break;
      }
    }
  }

  // Draw gray selection squares
  std::cerr << "selection size is: " << selection_.cells.size() << std::endl;
  cr->set_source_rgb(0.7, 0.7, 0.7);
  for (const CellInfo& cell_info : selection_.cells) {
    int oj = cell_info.column * (cell_size + border);
    int oi = cell_info.row * (cell_size + border);
    cr->rectangle(oj + border, oi + border, cell_size, cell_size);
  }
  cr->fill();

  // Draw red square for the current input cell
  cr->set_source_rgb(0.7, 0.0, 0.0);
  if (!selection_.cells.empty() &&
      selection_.current_cell != selection_.cells.end()) {
    int oj = selection_.current_cell->column * (cell_size + border);
    int oi = selection_.current_cell->row * (cell_size + border);
    cr->rectangle(oj + border, oi + border, cell_size, cell_size);
  }
  cr->fill();

  cr->set_source_rgb(0.0, 0.0, 0.0);
  for (Anchor& anchor : model_->GetAnchors()) {
    int oj = anchor.column * (cell_size + border);
    int oi = anchor.row * (cell_size + border);

    const double size = 10.0;
    const double hsize = size / 2.0;
    const double ratio = 0.3;

    const double scale = std::max(double(cell_size) / size, 1.0) * ratio;

    if (anchor.direction == Anchor::HORIZONTAL) {
      cr->move_to(oj + border, oi + border + (cell_size) / 3.0);
      cr->rel_line_to(hsize * scale, hsize * scale);
      cr->rel_line_to(-hsize * scale, hsize * scale);
      cr->close_path();
      cr->fill();
    } else {
      cr->move_to(oj + border + (cell_size) / 3.0, oi + border);
      cr->rel_line_to(hsize * scale, hsize * scale);
      cr->rel_line_to(hsize * scale, -hsize * scale);
      cr->close_path();
      cr->fill();
    }
  }

  Pango::FontDescription font;
  font.set_family("Arial Unicode");
  auto layout = create_pango_layout("");
  layout->set_font_description(font);

  cr->set_source_rgb(0.0, 0.0, 0.0);
  for (int i = 0; i < cells_vertical; ++i) {
    for (int j = 0; j < cells_horizontal; ++j) {
      if (model_->GetCell(i, j).is_clear)
        continue;

      int oj = j * (cell_size + border);
      int oi = i * (cell_size + border);

      const Glib::ustring text(1, model_->GetCell(i, j).character);
      layout->set_text(text);

      int text_width;
      int text_height;
      layout->get_pixel_size(text_width, text_height);

      const double ratio = 0.8;
      const double scale =
          std::max(double(cell_size) / text_height, 1.0) * ratio;

      cr->save();
      cr->translate(oj + border + (cell_size - text_width * scale) / 2,
                    oi + border + (cell_size - text_height * scale) / 2);

      cr->scale(scale, scale);

      layout->show_in_cairo_context(cr);
      cr->restore();
    }
  }

  return true;
}

bool CrosswordView::on_button_press_event(GdkEventButton* button_event) {
  if (!model_)
    return true;

  grab_focus();

  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();
  const int cells_horizontal = model_->GetWidth();
  const int cells_vertical = model_->GetHeight();
  const int border = 2;
  const int cell_size =
      std::min(width / cells_horizontal, height / cells_vertical) - border;

  int row = button_event->y / (cell_size + border);
  int column = button_event->x / (cell_size + border);

  std::cerr << "Button press event. Row: " << row << " Col: " << column
            << std::endl;

  if (row >= cells_vertical || column >= cells_horizontal)
    return true;

  const Selection::Direction direction =
      button_event->button == 1 ? Selection::HORIZONTAL : Selection::VERTICAL;
  selection_ = model_->GetSelection(row, column, direction);

  popover_.set_pointing_to(GetSelectionRectangle());
  // popover_.show();
  queue_draw();
  return true;
}

bool CrosswordView::on_key_press_event(GdkEventKey* key_event) {
  // todo: crash when pressing enter
  std::cerr << "Key press event: " << key_event->keyval << std::endl;

  if (selection_.cells.empty() ||
      selection_.current_cell == selection_.cells.end())
    return true;

  Selection new_selection;

  gunichar character = gdk_keyval_to_unicode(key_event->keyval);
  std::cerr << "The character is: " << character;

  switch (key_event->keyval) {
    case GDK_KEY_Shift_L:
    case GDK_KEY_Shift_R:
    case GDK_KEY_Alt_L:
    case GDK_KEY_Alt_R:
    case GDK_KEY_Control_L:
    case GDK_KEY_Return:
      return true;
    case GDK_KEY_BackSpace:
      model_->SetCharacter(selection_.current_cell->row,
                           selection_.current_cell->column, '.');
      model_->ClearCell(selection_.current_cell->row,
                        selection_.current_cell->column);
      if (selection_.current_cell != selection_.cells.begin())
        --selection_.current_cell;
      queue_draw();
      return true;
    case GDK_KEY_Delete:
      model_->SetCharacter(selection_.current_cell->row,
                           selection_.current_cell->column, '.');
      model_->ClearCell(selection_.current_cell->row,
                        selection_.current_cell->column);
      queue_draw();
      return true;
    case GDK_KEY_Left:
      new_selection = model_->GetSelection(selection_.current_cell->row,
                                           selection_.current_cell->column - 1,
                                           selection_.direction);
      break;
    case GDK_KEY_Right:
      new_selection = model_->GetSelection(selection_.current_cell->row,
                                           selection_.current_cell->column + 1,
                                           selection_.direction);
      break;
    case GDK_KEY_Up:
      new_selection = model_->GetSelection(selection_.current_cell->row - 1,
                                           selection_.current_cell->column,
                                           selection_.direction);
      break;
    case GDK_KEY_Down:
      new_selection = model_->GetSelection(selection_.current_cell->row + 1,
                                           selection_.current_cell->column,
                                           selection_.direction);
      break;
    default:
      if (character == 0)
        return true;
      model_->SetCharacter(selection_.current_cell->row,
                           selection_.current_cell->column, character);
      if (selection_.current_cell + 1 != selection_.cells.end())
        ++selection_.current_cell;
      queue_draw();
      return true;
  }

  if (!new_selection.cells.empty()) {
    std::cerr << "new selection size: " << new_selection.cells.size()
              << std::endl;
    selection_ = std::move(new_selection);
    popover_.set_pointing_to(GetSelectionRectangle());
    queue_draw();
    return true;
  }

  return true;
}

Gdk::Rectangle CrosswordView::GetSelectionRectangle() {
  Gtk::Allocation allocation = get_allocation();

  if (selection_.cells.empty() || !model_)
    return allocation;

  const int width = allocation.get_width();
  const int height = allocation.get_height();
  const int cells_horizontal = model_->GetWidth();
  const int cells_vertical = model_->GetHeight();
  const int border = 2;
  const int cell_size =
      std::min(width / cells_horizontal, height / cells_vertical) - border;

  CellInfo first = selection_.cells.front();
  CellInfo last = selection_.cells.back();

  int x = first.column * (cell_size + border);
  int y = first.row * (cell_size + border);

  int xx = (last.column + 1) * (cell_size + border);
  int yy = (last.row + 1) * (cell_size + border);

  return Gdk::Rectangle(x, y, xx - x, yy - y);
}

}  // namespace crossword_view
