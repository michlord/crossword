#include "crossword_model.h"

namespace crossword_view {

CrosswordModel::~CrosswordModel() {}

bool CrosswordModel::IsInRange(size_t row, size_t column) const {
  return row < GetHeight() && column < GetWidth();
}

std::ostream& operator<<(std::ostream& os, const CrosswordModel& model) {
  size_t width = model.GetWidth();
  size_t height = model.GetHeight();

  os << width << " " << height << std::endl;
  for (size_t row = 0; row < height; ++row) {
    for (size_t column = 0; column < width; ++column) {
      CellInfo cell = model.GetCell(row, column);
      if (cell.is_clear)
        os << "_";
      else if (cell.type == CellInfo::WALL)
        os << "#";
      else
        os << Glib::ustring(1, cell.character).raw();
    }
    os << std::endl;
  }

  return os;
}

}  // namespace crossword_view
