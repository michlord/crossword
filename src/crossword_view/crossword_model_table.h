#pragma once

#include "crossword_model.h"

#include <glibmm/ustring.h>
#include <vector>
#include <utility>

namespace crossword_view {

class CrosswordModelTable : public CrosswordModel {
 public:
  CrosswordModelTable(std::vector<gunichar> data, size_t width, size_t height);

  // CrosswordModel interface
  size_t GetWidth() const override;
  size_t GetHeight() const override;
  CellInfo GetCell(size_t row, size_t column) const override;
  void SetCharacter(size_t row, size_t column, gunichar character) override;
  Selection GetSelection(size_t row,
                         size_t column,
                         Selection::Direction direction) const override;
  std::vector<Anchor> GetAnchors() const override;
  std::vector<Anchor> GetVerticalAnchors() const override;
  std::vector<Anchor> GetHorizontalAnchors() const override;
  bool SetWord(const Anchor& anchor, const std::vector<gunichar>& word) override;
  std::vector<gunichar> GetWord(const Anchor& anchor) const override;

  void ClearCell(size_t row, size_t column) override;

  static CrosswordModelPtr CreateFromFile(const char* file_path);

 protected:
  std::vector<CellInfo> data_;

  std::vector<CellInfo> GetLine(size_t index, Anchor::Direction direction);
  std::vector<Anchor> MakeAnchors(Anchor::Direction direction);

  void RebuildAnchors();

  size_t width_;
  size_t height_;
  std::vector<Anchor> anchors_;
  std::vector<Anchor> vertical_anchors_;
  std::vector<Anchor> horizontal_anchors_;
};

}  // namespace crossword_view
